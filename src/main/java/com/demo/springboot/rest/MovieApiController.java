package com.demo.springboot.rest;

import com.demo.springboot.dto.CreateMovieDto;
import com.demo.springboot.dto.MovieDto;
import com.demo.springboot.dto.MovieListDto;
import com.demo.springboot.service.impl.MovieService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

@RestController
public class MovieApiController {
    private static final Logger LOG = LoggerFactory.getLogger(MovieApiController.class);

    @Autowired
    MovieService movieService;

    @GetMapping("/api/movies")
    public ResponseEntity<MovieListDto> getMovies() {
        LOG.info("--- get all movies: {}", movieService.sortMovies());
        return ResponseEntity.ok().body(movieService.sortMovies());    // = new ResponseEntity<>(movies, HttpStatus.OK);
    }

    @PostMapping("/api/movies")
    public ResponseEntity<Void> createMovie(@RequestBody CreateMovieDto createMovieDto) throws URISyntaxException {
        LOG.info("--- id: {}", createMovieDto.getTitle());
        LOG.info("--- title: {}", createMovieDto.getYear());
        LOG.info("--- image: {}", createMovieDto.getImage());
        if(createMovieDto.getTitle() != null && createMovieDto.getYear() != null && createMovieDto.getImage() != null && movieService.addNewFilm(createMovieDto)){
            movieService.addNewFilm(createMovieDto);
            return new ResponseEntity<>(HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @PutMapping("/api/movies/{id}")
    public ResponseEntity<Void> updateMovie( @PathVariable Integer id, @RequestBody CreateMovieDto updatedMovieDto) {
        LOG.info("--- id: {}", id);
        LOG.info("--- new title: {}", updatedMovieDto.getTitle());
        LOG.info("--- new year: {}", updatedMovieDto.getYear());
        LOG.info("--- new img: {}", updatedMovieDto.getImage());
        if(movieService.updateFilm(id, updatedMovieDto)){
            movieService.updateFilm(id, updatedMovieDto);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }


    @DeleteMapping("/api/movies/{id}")
    public ResponseEntity<Void> getMovie(@PathVariable("id") Integer id) {
        LOG.info("--- id: {}", id);
        if(movieService.deleteFilm(id)){
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }




}
