package com.demo.springboot.dto;

public class CreateMovieDto {
    private Integer year;
    private String title;
    private String image;

    public CreateMovieDto() {
    }

    public Integer getYear() {
        return year;
    }
    public String getImage(){ return image; }

    public String getTitle() {
        return title;
    }
}
