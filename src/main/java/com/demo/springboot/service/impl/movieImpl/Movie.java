package com.demo.springboot.service.impl.movieImpl;

import com.demo.springboot.dto.CreateMovieDto;
import com.demo.springboot.dto.MovieDto;
import com.demo.springboot.dto.MovieListDto;
import com.demo.springboot.service.impl.MovieService;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class Movie implements MovieService {
    private final MovieListDto movies;


    public Movie() {
        List<MovieDto> moviesList = new ArrayList<>();
        moviesList.add(new MovieDto(1,
                "Piraci z Krzemowej Doliny",
                1999,
                "https://fwcdn.pl/fpo/30/02/33002/6988507.6.jpg")
        );
        movies = new MovieListDto(moviesList);
    }

    public Movie(MovieListDto movies) {
        this.movies = movies;
    }

    public MovieListDto getMovies() {
        return movies;
    }

    @Override
    public boolean addNewFilm(CreateMovieDto createMovieDto) {
        Integer newId = movies.getMovies().size() + 1;
        movies.getMovies().add(new MovieDto(newId, createMovieDto.getTitle(), createMovieDto.getYear(), createMovieDto.getImage()));
        return true;
    }

    @Override
    public MovieListDto sortMovies() {
        Collections.sort(movies.getMovies(), Comparator.comparingInt(MovieDto::getMovieId));
        Collections.reverse(movies.getMovies());
        return movies;
    }

    @Override
    public boolean updateFilm(Integer id, CreateMovieDto createMovieDto) {
        ListIterator<MovieDto> iteFil = movies.getMovies().listIterator();
        while(iteFil.hasNext()){
            if(iteFil.next().getMovieId() == id){
                MovieDto temporary = iteFil.previous();
                temporary.setTitle(createMovieDto.getTitle());
                temporary.setYear(createMovieDto.getYear());
                temporary.setImage(createMovieDto.getImage());
                iteFil.set(temporary);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean deleteFilm(Integer id) {
        for(int i = 0; i < movies.getMovies().size() ; i++) {
            if (movies.getMovies().get(i).getMovieId().equals(id)) {
                movies.getMovies().removeIf(e -> e.getMovieId().equals(id));
                return true;
            }
        }
        return false;
    }


}
