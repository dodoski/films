package com.demo.springboot.service.impl;

import com.demo.springboot.dto.CreateMovieDto;
import com.demo.springboot.dto.MovieListDto;

public interface MovieService {
     MovieListDto getMovies();
     boolean addNewFilm(CreateMovieDto createMovieDto);
     MovieListDto sortMovies();
     boolean updateFilm(Integer id, CreateMovieDto createMovieDto);
     boolean deleteFilm(Integer id);
}
